package com.ruoyi.project.nczxwechat.setselecttime.service;

import java.util.List;
import com.ruoyi.project.nczxwechat.setselecttime.domain.WxSetselecttime;

/**
 * 选课时间设定Service接口
 * 
 * @author 阿卜QQ932696181
 * @date 2021-12-21
 */
public interface IWxSetselecttimeService 
{
    /**
     * 查询选课时间设定
     * 
     * @param id 选课时间设定主键
     * @return 选课时间设定
     */
    public WxSetselecttime selectWxSetselecttimeById(Long id);

    /**
     * 查询选课时间设定列表
     * 
     * @param wxSetselecttime 选课时间设定
     * @return 选课时间设定集合
     */
    public List<WxSetselecttime> selectWxSetselecttimeList(WxSetselecttime wxSetselecttime);

    /**
     * 新增选课时间设定
     * 
     * @param wxSetselecttime 选课时间设定
     * @return 结果
     */
    public int insertWxSetselecttime(WxSetselecttime wxSetselecttime);

    /**
     * 修改选课时间设定
     * 
     * @param wxSetselecttime 选课时间设定
     * @return 结果
     */
    public int updateWxSetselecttime(WxSetselecttime wxSetselecttime);

    /**
     * 批量删除选课时间设定
     * 
     * @param ids 需要删除的选课时间设定主键集合
     * @return 结果
     */
    public int deleteWxSetselecttimeByIds(String ids);

    /**
     * 删除选课时间设定信息
     * 
     * @param id 选课时间设定主键
     * @return 结果
     */
    public int deleteWxSetselecttimeById(Long id);

    /**
     * 通过学校ID查询当前学校选课设定时间
     * @param ognid
     * @return
     */
    public WxSetselecttime selectWxSetselecttimeBySchoolId(String ognid);
}
