package com.ruoyi.project.nczxwechat.setselecttime.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 选课时间设定对象 nczx_wx_setselecttime
 * 
 * @author 阿卜QQ932696181
 * @date 2021-12-21
 */
public class WxSetselecttime extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 学校代码 */
    @Excel(name = "学校代码")
    private String schoolid;

    /** 学校名称 */
    @Excel(name = "学校名称")
    private String schoolname;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date starttime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date endtime;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setSchoolid(String schoolid)
    {
        this.schoolid = schoolid;
    }

    public String getSchoolid()
    {
        return schoolid;
    }
    public void setSchoolname(String schoolname)
    {
        this.schoolname = schoolname;
    }

    public String getSchoolname()
    {
        return schoolname;
    }
    public void setStarttime(Date starttime)
    {
        this.starttime = starttime;
    }

    public Date getStarttime()
    {
        return starttime;
    }
    public void setEndtime(Date endtime)
    {
        this.endtime = endtime;
    }

    public Date getEndtime()
    {
        return endtime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("schoolid", getSchoolid())
            .append("schoolname", getSchoolname())
            .append("starttime", getStarttime())
            .append("endtime", getEndtime())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
