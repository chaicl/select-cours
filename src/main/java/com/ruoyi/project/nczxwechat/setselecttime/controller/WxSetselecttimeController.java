package com.ruoyi.project.nczxwechat.setselecttime.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.nczxwechat.setselecttime.domain.WxSetselecttime;
import com.ruoyi.project.nczxwechat.setselecttime.service.IWxSetselecttimeService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 选课时间设定Controller
 * 
 * @author 阿卜QQ932696181
 * @date 2021-12-21
 */
@Controller
@RequestMapping("/nczxwechat/setselecttime")
public class WxSetselecttimeController extends BaseController
{
    private String prefix = "nczxwechat/setselecttime";

    @Autowired
    private IWxSetselecttimeService wxSetselecttimeService;

    @RequiresPermissions("nczxwechat:setselecttime:view")
    @GetMapping()
    public String setselecttime()
    {
        return prefix + "/setselecttime";
    }

    /**
     * 查询选课时间设定列表
     */
    @RequiresPermissions("nczxwechat:setselecttime:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(WxSetselecttime wxSetselecttime)
    {
        startPage();
        List<WxSetselecttime> list = wxSetselecttimeService.selectWxSetselecttimeList(wxSetselecttime);
        return getDataTable(list);
    }

    /**
     * 导出选课时间设定列表
     */
    @RequiresPermissions("nczxwechat:setselecttime:export")
    @Log(title = "选课时间设定", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(WxSetselecttime wxSetselecttime)
    {
        List<WxSetselecttime> list = wxSetselecttimeService.selectWxSetselecttimeList(wxSetselecttime);
        ExcelUtil<WxSetselecttime> util = new ExcelUtil<WxSetselecttime>(WxSetselecttime.class);
        return util.exportExcel(list, "选课时间设定数据");
    }

    /**
     * 新增选课时间设定
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存选课时间设定
     */
    @RequiresPermissions("nczxwechat:setselecttime:add")
    @Log(title = "选课时间设定", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(WxSetselecttime wxSetselecttime)
    {
        return toAjax(wxSetselecttimeService.insertWxSetselecttime(wxSetselecttime));
    }

    /**
     * 修改选课时间设定
     */
    @RequiresPermissions("nczxwechat:setselecttime:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        WxSetselecttime wxSetselecttime = wxSetselecttimeService.selectWxSetselecttimeById(id);
        mmap.put("wxSetselecttime", wxSetselecttime);
        return prefix + "/edit";
    }

    /**
     * 修改保存选课时间设定
     */
    @RequiresPermissions("nczxwechat:setselecttime:edit")
    @Log(title = "选课时间设定", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(WxSetselecttime wxSetselecttime)
    {
        return toAjax(wxSetselecttimeService.updateWxSetselecttime(wxSetselecttime));
    }

    /**
     * 删除选课时间设定
     */
    @RequiresPermissions("nczxwechat:setselecttime:remove")
    @Log(title = "选课时间设定", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(wxSetselecttimeService.deleteWxSetselecttimeByIds(ids));
    }
}
