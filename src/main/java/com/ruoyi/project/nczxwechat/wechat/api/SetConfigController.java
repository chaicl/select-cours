package com.ruoyi.project.nczxwechat.wechat.api;

import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.nczxwechat.setselecttime.domain.WxSetselecttime;
import com.ruoyi.project.nczxwechat.setselecttime.service.IWxSetselecttimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.constraints.NotEmpty;

/***
 * author：阿卜QQ932696181
 * 配置相关制器
 */
@Controller
@RequestMapping("/wechat/api/user/")
public class SetConfigController {

    @Autowired
    IWxSetselecttimeService setselecttimeService;

    /**
     * 选课时间
     * @param ognid
     * @return
     */
    @GetMapping("getSetTime")
    @ResponseBody
    public AjaxResult getSetTime(@NotEmpty(message = "获取选课时间错误") String ognid){
        WxSetselecttime wxSetselecttime = setselecttimeService.selectWxSetselecttimeBySchoolId(ognid);
        return AjaxResult.success(wxSetselecttime);
    }
}
