package com.ruoyi.project.nczxwechat.wechat.api;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.project.nczxwechat.selectwhresult.domain.WxWhSelectresult;
import com.ruoyi.project.nczxwechat.selectwhresult.service.IWxWhSelectresultService;
import com.ruoyi.project.nczxwechat.stuinfo.domain.WxStuinfo;
import com.ruoyi.project.nczxwechat.stuinfo.service.IWxStuinfoService;
import com.ruoyi.project.nczxwechat.whcours.domain.WxWhcours;
import com.ruoyi.project.nczxwechat.whcours.service.IWxWhcoursService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import java.util.Date;
import java.util.List;

/***
 * author：阿卜QQ932696181
 * 课程相关控制器
 */
@Controller
@RequestMapping("/wechat/api/user/")
public class CourseController {

    @Autowired
    private IWxWhcoursService wxWhcoursService;

    @Autowired
    private IWxWhSelectresultService wxWhSelectresultService;

    @Autowired
    IWxStuinfoService stuinfoService;

    /**
     * 根据学校代码查询该学校设置的课程信息
     * @param schoolid
     * @return
     */
    @GetMapping("getWhCourseList")
    @ResponseBody
    public AjaxResult getWhCoursList(@NotEmpty(message = "获取课程信息错误") String schoolid){
        List<WxWhcours> wxWhcoursres = wxWhcoursService.selectWxWhcoursByShoolid(schoolid);
//        System.out.println(wxWhcoursres);
        return  AjaxResult.success(wxWhcoursres);
    }

    /**
     * 开始选课
     * @param selectinfo
     * @return
     */
    @PostMapping("selectCourse")
    @ResponseBody
    public AjaxResult selectCourse(@NotEmpty(message = "接收选课信息失败") @RequestBody String selectinfo){
        Date date=new Date();
        JSONObject json = JSONObject.parseObject(selectinfo);
        WxStuinfo wxStuinfos = stuinfoService.selectWxStuinfoByStuNo(json.get("stuno").toString());
//        System.out.println(wxStuinfos);
        WxWhSelectresult wxWhSelectresult = new WxWhSelectresult();
        wxWhSelectresult.setStuno(json.get("stuno").toString());
        wxWhSelectresult.setSchooid(wxStuinfos.getSchooid());
        wxWhSelectresult.setSchoolname(wxStuinfos.getSchoolname());
        wxWhSelectresult.setStuname(wxStuinfos.getStuname());
        wxWhSelectresult.setStusex(wxStuinfos.getStusex());
        wxWhSelectresult.setStuclass(wxStuinfos.getStuclass());
        wxWhSelectresult.setOpenId(wxStuinfos.getOpenId());
        wxWhSelectresult.setSelectinfo(json.get("selectinfo").toString());
        wxWhSelectresult.setCreateBy(wxStuinfos.getStuno());
        wxWhSelectresult.setCreateTime(date);
        if (wxStuinfos.getStatues()==1){
            return AjaxResult.error("您已选课，请勿重复选课！");
        }else{
            //选课数据插入
            wxWhSelectresultService.insertWxWhSelectresult(wxWhSelectresult);
            //更新学生选课状态-选课
            stuinfoService.updateWxStuinfoSelectStateByStuNo(json.get("stuno").toString());
            return AjaxResult.success();
        }
    }
    /**
     * 通过学生唯一编号查询选课信息
     * @param stuno
     * @return
     */
    @GetMapping("loadMyCoursList")
    @ResponseBody
    public AjaxResult loadMyCoursList(@NotEmpty(message = "接收学生信息失败") String stuno){
        System.out.println(stuno);
        WxWhSelectresult wxWhSelectresult = wxWhSelectresultService.selectWxWhSelectresultByStuno(stuno);
        return AjaxResult.success(wxWhSelectresult);
    }
    /**
     * 开始退选课
     * @param stuinfo
     * @return
     */
    @PostMapping("cancelCourse")
    @ResponseBody
    public AjaxResult cancelCourse(@NotEmpty(message = "接收选课信息失败") @RequestBody String stuinfo) {
        JSONObject json = JSONObject.parseObject(stuinfo);
        //删除原先的选科数据
        wxWhSelectresultService.deleteWxWhSelectresultByStuno(json.get("stuno").toString());
        //更新学生选课状态-未选课
        stuinfoService.updateWxStuinfoSelectStateByStuNo1(json.get("stuno").toString());
        return  AjaxResult.success();
    }
}
