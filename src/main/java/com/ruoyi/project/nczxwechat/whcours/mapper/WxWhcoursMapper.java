package com.ruoyi.project.nczxwechat.whcours.mapper;

import java.util.List;
import com.ruoyi.project.nczxwechat.whcours.domain.WxWhcours;

/**
 * 文化课程信息Mapper接口
 * 
 * @author 阿卜QQ932696181
 * @date 2021-12-20
 */
public interface WxWhcoursMapper 
{
    /**
     * 查询文化课程信息
     * 
     * @param id 文化课程信息主键
     * @return 文化课程信息
     */
    public WxWhcours selectWxWhcoursById(Long id);

    /**
     * 查询文化课程信息列表
     * 
     * @param wxWhcours 文化课程信息
     * @return 文化课程信息集合
     */
    public List<WxWhcours> selectWxWhcoursList(WxWhcours wxWhcours);

    /**
     * 新增文化课程信息
     * 
     * @param wxWhcours 文化课程信息
     * @return 结果
     */
    public int insertWxWhcours(WxWhcours wxWhcours);

    /**
     * 修改文化课程信息
     * 
     * @param wxWhcours 文化课程信息
     * @return 结果
     */
    public int updateWxWhcours(WxWhcours wxWhcours);

    /**
     * 删除文化课程信息
     * 
     * @param id 文化课程信息主键
     * @return 结果
     */
    public int deleteWxWhcoursById(Long id);

    /**
     * 批量删除文化课程信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWxWhcoursByIds(String[] ids);
    /**
     * 根据学校ID查询学设置的课程列表
     * @param schoolid
     * @return
     */
    public List<WxWhcours>  selectWxWhcoursByShoolid(String schoolid);
}
