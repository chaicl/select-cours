package com.ruoyi.project.nczxwechat.whcours.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 文化课程信息对象 nczx_wx_whcours
 * 
 * @author 阿卜QQ932696181
 * @date 2021-12-20
 */
public class WxWhcours extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    @Excel(name = "id")
    private Long id;

    /** 学校代码 */
    @Excel(name = "学校代码")
    private String schoolid;

    /** 学校名称 */
    @Excel(name = "学校名称")
    private String schoolName;

    /** 课程id */
    @Excel(name = "课程id")
    private String courseType;

    /** 课程名称 */
    @Excel(name = "课程名称")
    private String courseName;

    /** 教师Id */
    private String teacherid;

    /** 授课教师 */
    @Excel(name = "授课教师")
    private String techaer;

    /** 状态 */
    @Excel(name = "状态")
    private Long statues;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setSchoolid(String schoolid)
    {
        this.schoolid = schoolid;
    }

    public String getSchoolid()
    {
        return schoolid;
    }
    public void setSchoolName(String schoolName)
    {
        this.schoolName = schoolName;
    }

    public String getSchoolName()
    {
        return schoolName;
    }
    public void setCourseType(String courseType)
    {
        this.courseType = courseType;
    }

    public String getCourseType()
    {
        return courseType;
    }
    public void setCourseName(String courseName)
    {
        this.courseName = courseName;
    }

    public String getCourseName()
    {
        return courseName;
    }
    public void setTeacherid(String teacherid)
    {
        this.teacherid = teacherid;
    }

    public String getTeacherid()
    {
        return teacherid;
    }
    public void setTechaer(String techaer)
    {
        this.techaer = techaer;
    }

    public String getTechaer()
    {
        return techaer;
    }
    public void setStatues(Long statues)
    {
        this.statues = statues;
    }

    public Long getStatues()
    {
        return statues;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("schoolid", getSchoolid())
            .append("schoolName", getSchoolName())
            .append("courseType", getCourseType())
            .append("courseName", getCourseName())
            .append("teacherid", getTeacherid())
            .append("techaer", getTechaer())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("statues", getStatues())
            .toString();
    }
}
