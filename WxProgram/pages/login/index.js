import util from '../../utils/util';
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    token:'',
    ischek:0,
    captchacode:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function () {
    
  },

  onShow: function () {
    //验证码
    this.createCode();
  },
  //验证码切换
  nextCode(){
    this.createCode()
  },
  //创建验证码
  createCode() {
    var code;
    //首先默认code为空字符串
    code = '';
    //设置长度，这里看需求，我这里设置了4
    var codeLength = 4;
    //设置随机字符
    var random = new Array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z','a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
    //循环codeLength 我设置的4就是循环4次
    for (var i = 0; i < codeLength; i++) {
      //设置随机数范围,这设置为0 ~ 36
      var index = Math.floor(Math.random() * 62);
      //字符串拼接 将每次随机的字符 进行拼接
      code += random[index];
    }
    //将拼接好的字符串赋值给展示的code
    this.setData({
      captchacode: code
    })
  },
  //获取学生信息
  confrimInfo:function(e){
    wx.showLoading({title: '请稍后...'})
    // stuno:学生输入的学号 inputcaptchacode:用户输入的验证码 captchacode:验证码
    const {stuno, inputcaptchacode} = e.detail.value;
    if (stuno.length==0) {
      wx.showToast({
        title: '请输入学生ID',
        icon: 'error', 
        duration: 2000
      })
      return false;
    }
    if(inputcaptchacode.length==0){
      wx.showToast({
        title: '请输入验证码',
        icon: 'error',
        duration: 2000
      })
      return false;
    }
    if(('' + inputcaptchacode).toUpperCase()!=this.data.captchacode.toUpperCase()){
      wx.showToast({
        title: '验证码不正确',
        icon: 'error',
        duration: 2000
      })
      this.nextCode();
    }else{
      // TIPS: 这个接口直接返回userInfo。如果成功返回就存在userInfo里了，这个stuno是干啥的。然后直接跳转进去了。就登录成功了。
      util.get(`getUserInfo?stuno=${stuno}`,response=>{
        console.log(response)
        // TIPS: 为0不一般是失败么
        if (response.code==0) {
          wx.hideLoading() //关闭提示框
          wx.setStorageSync('stuno', response.data.stuno);
          this.setData({
            //显示学校基本信息
            ischek:2,
            ...response.data,
          })
        }else{
          wx.showToast({
            title: response.msg,
            icon: 'none',
            duration: 2000
          })
        }
      })
    }
  },
  //获取微信用户信息
  wxlogin:function(){
    wx.getUserProfile({
      desc: '用于完善会员资料',
      success:(res)=>{
        if(res){
          wx.showLoading({title: '确认中...'})
          console.log("获取微信用户信息：",res)
          let code = null;
          let stuno = wx.getStorageSync("stuno");
          console.log("stuno",stuno)
          wx.login({
            success: function (e) {
                code = e.code
                let params = {};
                params.code = code; //用户code  注:用户的code每次登录都是随机的，所以不需要进行存储
                params.avatarUrl = res.userInfo.avatarUrl; //用户头像
                params.nickName = res.userInfo.nickName; //用户微信名
                params.stuno = stuno; //学生唯一ID
                util.post('wxlogin',params,response=>{
                  const data = response
                  if (data.code === 0) {
                    console.log("----登录成功----",data)
                    //存储用户信息
                    wx.setStorageSync('userInfo', data.data);
                    wx.setStorageSync('token', data.data.openId);
                    wx.hideLoading() //关闭提示框
                    wx.switchTab({
                      url: '/pages/user/index/index', //跳转到指定页面
                    })
                  }else{
                    wx.showToast({
                      title: data.msg,
                      icon: 'none',
                      duration: 2000
                    })
                  }
               })
            },fail(err) {
              wx.showToast({
                title: '请求失败，请检查网络，或与相关人员联系',
                icon: 'none',
                duration: 2000
              })
            }
          })  
        }    
      },
      fail:function(e){
        console.log("拒绝登陆了",e)
        wx.showToast({
          title: '您授权登录失败了！',
          icon: 'error',
          duration: 2000
        })
      }
    })
  },
  //重置所有
  resetAll:function(){
    this.setData({
      ischek:0
    })
  },
})