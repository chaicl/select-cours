var api = require('../../utils/api.js')
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    checklogin: true,
    captchaImage: undefined,
    uuid: undefined,
    username: undefined,
    password: undefined,
    captcha: undefined,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function () {},
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let that = this;
    //免登录
    const userinfo = wx.getStorageSync('userInfo')
    console.log(userinfo)
    if (userinfo != null && userinfo != '') {
      wx.navigateTo({
        url: '/pages/index/index',
      })
    }
  },

  bindGetUserInfo(e) {
    let that = this
    //用户还未登录，申请用户授权
    wx.getUserProfile({
      desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        console.log("userinfores----", res)
        if (res.errMsg == "getUserProfile:ok") {
          let code = null
          wx.login({
            success: function (e) {
              code = e.code
              let params = {};
              params.code = code; //用户code  注:用户的code每次登录都是随机的，所以不需要进行存储
              params.avatarUrl = res.userInfo.avatarUrl; //用户头像
              params.nickName = res.userInfo.nickName; //用户微信名
              wx.request({
                url: `${app.globalData.host}` + 'getUserinfo', //后台接口
                data: params,
                method: 'POST',
                header: {
                  'Content-Type': 'application/json',
                  'X-Nideshop-Token': wx.getStorageSync('token')
                },
                success: function (res) {
                  console.log(res)
                  if (res.data.code === 200) {
                    //存储用户信息
                    wx.setStorageSync('userInfo', res.data.userInfo);
                    wx.setStorageSync('token', res.data.userInfo.openId);
                    wx.navigateTo({
                      url: '/pages/index/index', //跳转到指定页面
                    })
                    wx.hideLoading() //关闭提示框
                  } else {
                    //输出错误信息
                    wx.showToast({
                      title: '登录失败咯',
                      icon: 'error'
                    })
                    wx.hideLoading() //关闭提示框
                  }
                },
                fail(err) {
                  console.log(err)
                  wx.showToast({
                    title: '网络连接失败',
                    icon: 'error'
                  })
                  wx.hideLoading() //关闭提示框
                }
              })
            },
            fail(err) {
              console.log(err)
            }
          })
        } else {
          wx.hideLoading() //关闭提示框
          //用户按了拒绝按钮
          wx.showModal({
            title: '警告通知',
            content: '您点击了拒绝授权,将无法正常显示个人信息,点击确定重新获取授权。',
            success: function (res) {
              //用户拒绝登录后的处理
            }
          });
        }
      }
    })
  },
})