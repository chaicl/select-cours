import util from '../../utils/util';
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    token:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function () {
    
  },

  onShow: function () {
    let that = this;
    const userinfo = wx.getStorageSync('userInfo');
    const token = wx.getStorageSync('token');
    console.log(userinfo)
    if (userinfo != null && userinfo != '') {
      wx.switchTab({
        url: '/pages/index/index',
      })
    }
  },

  bindGetUserInfo(e) {
    let that = this
    //用户还未登录，申请用户授权
    wx.getUserProfile({
      desc: '用于完善会员资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: (res) => {
        // console.log("userinfores----", res)
        if (res.errMsg == "getUserProfile:ok") {
          let code = null
          wx.login({
            success: function (e) {
              code = e.code
              let params = {};
              params.code = code; //用户code  注:用户的code每次登录都是随机的，所以不需要进行存储
              params.avatarUrl = res.userInfo.avatarUrl; //用户头像
              params.nickName = res.userInfo.nickName; //用户微信名
            util.post('wxlogin',params,response=>{
              wx.showLoading({
                title: '登陆中...',
                mask:true
              })
              const data = response
              if (data.code === 200) {
                console.log("----登录成功----",data)
                //存储用户信息
                wx.setStorageSync('userInfo', data.userInfo);
                wx.setStorageSync('token', data.userInfo.openId);
                wx.hideLoading() //关闭提示框
                // wx.switchTab({
                //   url: '/pages/user/index/index', //跳转到指定页面
                // })
              }
            }
            )
            },
            fail(err) {
              console.log(err)
            }
          })
        } else {
          wx.hideLoading() //关闭提示框
          //用户按了拒绝按钮
          wx.showModal({
            title: '警告通知',
            content: '您点击了拒绝授权,将无法正常显示个人信息,点击确定重新获取授权。',
            success: function (res) {
              //用户拒绝登录后的处理
            }
          });
        }
      }
    })
  },
})