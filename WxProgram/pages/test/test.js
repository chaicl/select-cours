// pages/test/test.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    endeTime:'2021-12-22 10:00:00',
  },

  onLoad: function (options) {
    var that = this;
    that.countDown(this.data.endeTime)
  },
  /**
   * 倒计时
   * 
   * @param endTime 结束日期+时间
   * **/
   countDown: function (endTime) {
    var that = this;
    that.setData({
      timer: setInterval(function () { //周期计时器，每隔1秒执行一次方法里的代码
        //得到一个从现在时间开始到活动结束的时间戳 
        var downTime = parseInt(new Date(endTime.replace(/-/g, "/")).getTime() - new Date().getTime());
        // 倒计时结束
        if (downTime <= 0) {
          that.setData({
            day: '00',
            hour: '00',
            minute: '00',
            second: '00'
          })
          //结束周期计时器
          clearInterval(that.data.timer);
          return;
        }
        //计算距离活动还有多少天、时、分、秒
        var d = parseInt(downTime / 1000 / 3600 / 24);
        var h = parseInt(downTime / 1000 / 3600 % 24);
        var m = parseInt(downTime / 1000 / 60 % 60);
        var s = parseInt(downTime / 1000 % 60);
        //统一格式的显示
        d < 10 ? d = '0' + d : d;
        h < 10 ? h = '0' + h : h;
        m < 10 ? m = '0' + m : m;
        s < 10 ? s = '0' + s : s;
        //同步显示
        that.setData({
          day: d,
          hour: h,
          minute: m,
          second: s
        })
      }, 1000)
    })
  },
})