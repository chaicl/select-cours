let outTime=null
App({
  onLaunch: function() {
    //版本升级提示
    this.upgradeTip();
    wx.getSystemInfo({
      success: e => {
        this.globalData.StatusBar = e.statusBarHeight;
        let capsule = wx.getMenuButtonBoundingClientRect();
        if (capsule) {
          this.globalData.Custom = capsule;
          this.globalData.CustomBar = capsule.bottom + capsule.top - e.statusBarHeight;
        } else {
          this.globalData.CustomBar = e.statusBarHeight + 70;
        }
      }
    })
  },
  //版本升级提示
  upgradeTip:function(){
    if (wx.canIUse('getUpdateManager')) {
      const updateManager = wx.getUpdateManager();
      // 监听向微信后台请求检查更新结果事件
      updateManager.onCheckForUpdate(function (res) {
        //如果hasUpdate为true表示有版本更新
        if (res.hasUpdate) {
          //小程序有版本更新事件回调
          updateManager.onUpdateReady(function () {
            wx.showModal({
              title: '更新提示',
              content: '新版本已经准备好，是否重启应用？',
              success: function (res) {
                if (res.confirm) {
                  //强制小程序重启并使用新版本
                  updateManager.applyUpdate()
                }
              }
            })
          })
          //监听小程序更新失败事件
          updateManager.onUpdateFailed(function () {
            wx.showModal({
              title: '已经有新版本了哟~',
              content: '新版本已经上线啦~，请您删除当前小程序，重新搜索打开哟~'
            })
          })
        }
      })
      
    } else {
      wx.showModal({
        title: '提示',
        content: '当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。'
      })
    }
  },
  globalData: {
    userInfo: null,
    host: 'http://localhost:8080/tlv8/wechat/api/',
  },
})
